//
//  LabelAnimation.swift
//  MovieReviewsReader
//
//  Created by David Halapir on 28/02/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation
import UIKit

protocol CanEnterScreenFromSides {}
protocol CanStayHiddenForGivenTime {}
protocol CanFadeOut {}

extension CanEnterScreenFromSides where Self: UIView {
    func enterScreenFromRightToLeft() {
        transform = CGAffineTransform(translationX: UIScreen.main.bounds.width, y: 0)

        UIView.animate(withDuration: 0.34,
                       delay: 0.0,
                       options: .curveEaseOut,
                       animations: { self.transform = CGAffineTransform.identity },
                       completion: nil)
    }
}

extension CanStayHiddenForGivenTime where Self: UIView {
    func stayHiddenFor(duration: Double) {
        self.isUserInteractionEnabled = false
        self.alpha = 0.0
        UIView.animate(withDuration: duration,
                       delay: 0.0,
                       options: .curveEaseIn,
                       animations: {self.alpha = 1.0},
                       completion: {[weak self] finished in
                            self?.isUserInteractionEnabled = true
                        })
    }
}

class MovieDetailLabel: UILabel, CanEnterScreenFromSides, CanStayHiddenForGivenTime {}
