//
//  MovieDetailViewController.swift
//  MovieReviewsReader
//
//  Created by David Halapir on 28/02/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import PureLayout
import CoreData

class MovieDetailViewController: UIViewController {
    
    // MARK: - Model
    var review: MovieReview?
    var movieEntity: Movie?
    var delegate: ButtonDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImage()
        loadLabels()
        loadComment()
    }

    // MARK: - Properties
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: MovieDetailLabel!
    @IBOutlet weak var criticNameLabel: MovieDetailLabel!
    @IBOutlet weak var dateUpdatedLabel: MovieDetailLabel!
    @IBOutlet weak var shortSummaryLabel: MovieDetailLabel!
    @IBOutlet weak var browserLinkLabel: MovieDetailLabel! {
        didSet {
            browserLinkLabel.isUserInteractionEnabled = true
            let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(userTappedLinkLabel(_:)))
            browserLinkLabel.addGestureRecognizer(tapRecognizer)
        }
    }
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var commentLabel: UILabel! {
        didSet {
            let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(userTappedCommentLabel(_:)))
            commentLabel.addGestureRecognizer(tapRecognizer)
        }
    }
    
    // MARK: - Button outlet
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var editButton: UIButton!

    // MARK: - Button actions
    @IBAction func removeButton(_ sender: UIButton) {
        movieEntity?.comment = nil
        removeButton.isHidden = true
        editButton.isHidden = true
        commentLabel.setToPlaceholderMode()
        CoreDataController.saveContext()
        delegate?.userDidSaveOrDeleteComment()
    }
    
    @IBAction func editButton(_ sender: UIButton) {
        let commentvc = CommentViewController()
        commentvc.delegate = self
        commentvc.movieEntity = movieEntity
        commentvc.commentTextView.text = movieEntity?.comment?.text
        navigationController?.pushViewController(commentvc, animated: true)
    }

    // MARK: - Private functions
    private func loadImage() {
        spinner.startAnimating()
        guard let currentReview = review else { return }
        DispatchQueue.global(qos: .userInitiated).async {
            guard
                let movieImageUrl = URL(string: currentReview.mediaUrl),
                let imageData = try? Data(contentsOf: movieImageUrl),
                let image = UIImage(data: imageData)?.scaledTo(size: self.movieImageView.bounds.size)
            else { return }
            DispatchQueue.main.async {
                self.spinner.stopAnimating()
                if currentReview == self.review {
                    self.movieImageView.image = image
                }
            }
        }
    }
    
    private func loadLabels() {
        guard let currentReview = review else { return }
        movieTitleLabel.text = currentReview.title
        criticNameLabel.text = currentReview.reviewerName
        dateUpdatedLabel.text = currentReview.dateUpdated.components(separatedBy: " ").first
        shortSummaryLabel.text = currentReview.summary
        
        movieTitleLabel.enterScreenFromRightToLeft()
        criticNameLabel.enterScreenFromRightToLeft()
        dateUpdatedLabel.enterScreenFromRightToLeft()
        shortSummaryLabel.enterScreenFromRightToLeft()
        browserLinkLabel.stayHiddenFor(duration: 1.0)
    }
    
    fileprivate func loadComment() {
        let request: NSFetchRequest<Movie> = Movie.fetchRequest()
        let predicate = NSPredicate(format: "title == %@", (review?.title)!)
        request.predicate = predicate
        do {
            let movie = try CoreDataController.getContext().fetch(request).first
            movieEntity = movie
            if let comment = movie?.comment {
                commentLabel.removePlaceholder()
                commentLabel.text = comment.text
                commentLabel.isUserInteractionEnabled = false
                editButton.isHidden = false
                removeButton.isHidden = false
                delegate?.userDidSaveOrDeleteComment()
            } else {
                commentLabel.setToPlaceholderMode()
            }
        } catch let error { print(error.localizedDescription) }
    }
    
    // MARK: - Tap Handlers
    
    func userTappedLinkLabel(_ gestureRecognizer: UITapGestureRecognizer) {
        guard
            let urlString = review?.webUrl,
            let url = URL(string: urlString)
        else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    func userTappedCommentLabel(_ gestureRecognizer: UITapGestureRecognizer) {
        if commentLabel.isInPlaceholderMode() {
            let commentvc = CommentViewController()
            commentvc.movieEntity = movieEntity
            commentvc.delegate = self
            navigationController?.pushViewController(commentvc, animated: true)
        }
    }

}

extension MovieDetailViewController: ButtonDelegate {
    func userDidSaveOrDeleteComment() {
        loadComment()
    }
}

extension UIImage {
    func scaledTo(size: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}

extension UILabel {
    func setToPlaceholderMode() {
        self.text = "tap to add comment"
        self.textColor = UIColor.lightGray
        self.isUserInteractionEnabled = true
    }
    func isInPlaceholderMode() -> Bool {
        if self.textColor == UIColor.lightGray {
            return true
        }
        return false
    }
    func removePlaceholder() {
        self.textColor = UIColor.black
        self.text = ""
    }
}



