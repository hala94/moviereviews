//
//  MovieViewController.swift
//  MovieReviewsReader
//
//  Created by David Halapir on 24/02/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import Alamofire
import PureLayout
import SwiftyJSON
import CoreData


let apiUrl = "https://api.nytimes.com/svc/movies/v2/reviews/search.json?api-key=0673d61ce89b4ea4889932a53181233f"

class MovieViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - Model
    private var reviews = [MovieReview]()
    
    fileprivate var tableView = UITableView()
    private var cache = [IndexPath: UIImage]()
    fileprivate var selectedIndexPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        view.isOpaque = true
        navigationItem.title = "Movies"
        configureTableView()
        fetchReviews()
    }
    
    
    // MARK: - TableView data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieCell", for: indexPath)
        guard
            let movieCell = cell as? MovieTableViewCell
        else { return cell }
        let movieReview = reviews[indexPath.row]
        
        let request: NSFetchRequest<Movie> = Movie.fetchRequest()
        let predicate = NSPredicate(format: "title == %@", movieReview.title)
        request.predicate = predicate
        do {
            let movie = try CoreDataController.getContext().fetch(request).first
            if movie?.value(forKey: "comment") != nil { movieCell.commentedLabel.isHidden = false }
            else { movieCell.commentedLabel.isHidden = true }
        } catch let error { print(error.localizedDescription) }
  
        movieCell.movieNameLabel.text = movieReview.title
        movieCell.movieImageView.image = nil
        movieCell.spinner.startAnimating()
        if let storedImage = cache[indexPath] {
            movieCell.spinner.stopAnimating()
            movieCell.movieImageView.image = storedImage
        } else {
            DispatchQueue.global(qos: .background).async {
                guard
                    let url = URL(string: movieReview.mediaUrl),
                    let imageData = try? Data(contentsOf: url)
                    else { return }
                let image = UIImage(data: imageData)?.withRoundedCornersAndSize(size: movieCell.movieImageView.bounds.size)
                self.cache[indexPath] = image
                DispatchQueue.main.async {
                    movieCell.movieImageView.image = image
                    movieCell.spinner.stopAnimating()
                }
            }
        }
        return cell
    }
    
    // MARK: - Private functions
    private func configureTableView() {
        view.addSubview(tableView)
        tableView.backgroundColor = UIColor.white
        tableView.isOpaque = true
        tableView.separatorStyle = .none
        let nib = UINib(nibName: "MovieTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "movieCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 80
        automaticallyAdjustsScrollViewInsets = false
        tableView.autoPin(toTopLayoutGuideOf: self, withInset: 0)
        tableView.autoPin(toBottomLayoutGuideOf: self, withInset: 0)
        tableView.autoPinEdge(toSuperviewEdge: .leading)
        tableView.autoPinEdge(toSuperviewEdge: .trailing)
    }
    
    // MARK: - Networking
    func fetchReviews() {
        Alamofire.request(apiUrl).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let movieReviewsJson = JSON(value)
                let reviewsArray = movieReviewsJson["results"].arrayValue
                let toMovieReviews = MovieReview.toMovieReviews
                let fetchedReviews = reviewsArray.map(toMovieReviews)
                self.reviews = fetchedReviews
                self.updateDatabase(fetchedReviews)
                self.tableView.separatorStyle = .singleLine
                self.tableView.reloadData()
            case .failure(let error):
                self.displayAlert(message: error.localizedDescription)
            }
        }
    }
    
    // MARK: - CoreData - updating database while avoiding duplicates
    private func updateDatabase(_ reviews: [MovieReview]) {
        CoreDataController.getContext().perform {
            for review in reviews {
                let request: NSFetchRequest<Movie> = Movie.fetchRequest()
                let predicate = NSPredicate(format: "title == %@", review.title)
                request.predicate = predicate
                do {
                    let count = try CoreDataController.getContext().count(for: request)
                    if count == 0 {
                        let movie = NSEntityDescription.insertNewObject(forEntityName: "Movie", into: CoreDataController.getContext()) as? Movie
                        movie?.title = review.title
                    }
                } catch let error {
                    print(error.localizedDescription)
                }
            }
            CoreDataController.saveContext()
        }
    }
    
    // MARK: - Navigation
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndexPath = indexPath
        tableView.deselectRow(at: indexPath, animated: true)
        let selectedMovie = reviews[indexPath.row]
        let movieDetailvc = MovieDetailViewController()
        movieDetailvc.review = selectedMovie
        movieDetailvc.delegate = self
        navigationController?.pushViewController(movieDetailvc, animated: true)
    }
    
}

extension UIImage {
    func withRoundedCornersAndSize(size: CGSize) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        UIGraphicsGetCurrentContext()?.addPath(UIBezierPath(roundedRect: rect, cornerRadius: 10.0).cgPath)
        UIGraphicsGetCurrentContext()?.clip()
        draw(in: rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}

extension UIViewController {
    func displayAlert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Try again",
                                     style: .default,
                                     handler: {alert in
                                        guard let movievc = self as? MovieViewController else { return }
                                        movievc.fetchReviews()
                                    })
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

extension MovieViewController: ButtonDelegate {
    func userDidSaveOrDeleteComment() {
        guard let indexPath = selectedIndexPath else { return }
        tableView.reloadRows(at: [indexPath], with: .fade)
    }
}
