//
//  Movie+CoreDataProperties.swift
//  MovieReviewsReader
//
//  Created by David Halapir on 09/03/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation
import CoreData


extension Movie {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Movie> {
        return NSFetchRequest<Movie>(entityName: "Movie");
    }

    @NSManaged public var title: String?
    @NSManaged public var comment: Comment?

}

extension Movie {
    
    @NSManaged public func addCommentObject(_ value: Comment)
    @NSManaged public func removeCommentObject(_ value: Comment)
}

