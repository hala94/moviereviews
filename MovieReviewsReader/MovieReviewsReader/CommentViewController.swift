//
//  CommentViewController.swift
//  MovieReviewsReader
//
//  Created by David Halapir on 10/04/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import PureLayout
import CoreData

class CommentViewController: UIViewController {
    
    var commentTextView = UITextView()
    private var saveBarButton = UIBarButtonItem()
    var movieEntity: Movie?
    var delegate: ButtonDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        commentTextView.translatesAutoresizingMaskIntoConstraints = false
        commentTextView.autocorrectionType = .no
        commentTextView.autocapitalizationType = .none
        commentTextView.font = UIFont(name: "Avenir Next Regular", size: 14)
        commentTextView.becomeFirstResponder()
        view.addSubview(commentTextView)
        commentTextView.autoPin(toTopLayoutGuideOf: self, withInset: 0, relation: .equal)
        commentTextView.autoPin(toBottomLayoutGuideOf: self, withInset: 0)
        commentTextView.autoPinEdge(toSuperviewEdge: .trailing)
        commentTextView.autoPinEdge(toSuperviewEdge: .leading)
        saveBarButton.target = self
        saveBarButton.title = "Save"
        saveBarButton.action = #selector(handleSave)
        saveBarButton.style = .done
        navigationItem.rightBarButtonItem = saveBarButton
        
    }
    
    @objc private func handleSave() {
        let comment = NSEntityDescription.insertNewObject(forEntityName: "Comment", into: CoreDataController.getContext()) as! Comment
        comment.text = commentTextView.text
        commentTextView.isEditable = false
        movieEntity?.comment = comment
        CoreDataController.saveContext()
        delegate?.userDidSaveOrDeleteComment()
        _ = navigationController?.popViewController(animated: true)
    }

}
