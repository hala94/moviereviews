//
//  MovieReview.swift
//  MovieReviewsReader
//
//  Created by David Halapir on 25/02/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation
import SwiftyJSON

struct MovieReview {
    let title: String
    let reviewerName: String
    let summary: String
    let dateUpdated: String
    let webUrl: String
    let mediaUrl: String

    static let toMovieReviews: (JSON) -> MovieReview = { review in
        return MovieReview(
            title: review["display_title"].stringValue,
            reviewerName: review["byline"].stringValue,
            summary: review["summary_short"].stringValue,
            dateUpdated: review["date_updated"].stringValue,
            webUrl: review["link"]["url"].stringValue,
            mediaUrl: review["multimedia"]["src"].stringValue
        )
    }
}

extension MovieReview: Equatable {
    static func ==(lhs: MovieReview, rhs: MovieReview) -> Bool {
        let isEqual = lhs.title==rhs.title
        return isEqual
    }
}

