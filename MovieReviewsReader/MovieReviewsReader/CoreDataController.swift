//
//  CoreDataController.swift
//  MovieReviewsReader
//
//  Created by David Halapir on 09/03/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation
import CoreData

class CoreDataController {
    
    // MARK: - Core Data stack
    
    static var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    class func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    class func getContext() -> NSManagedObjectContext {
        return persistentContainer.viewContext
    }
}

//extension CoreDataController {
//    static func deleteAllData(entity: String) {
//        let context = CoreDataController.persistentContainer.viewContext
//        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
//        fetchRequest.returnsObjectsAsFaults = false
//        
//        do
//        {
//            let results = try context.fetch(fetchRequest)
//            for managedObject in results
//            {
//                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
//                context.delete(managedObjectData)
//            }
//        } catch let error as NSError {
//            print("Delete all data in \(entity) error : \(error) \(error.userInfo)")
//        }
//    }
//}
