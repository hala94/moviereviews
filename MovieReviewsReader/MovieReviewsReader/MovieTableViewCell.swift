//
//  MovieTableViewCell.swift
//  MovieReviewsReader
//
//  Created by David Halapir on 25/02/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class MovieTableViewCell: UITableViewCell {
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var movieNameLabel: UILabel!
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var commentedLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        movieImageView.image = nil
    }
}


