//
//  Comment+CoreDataProperties.swift
//  MovieReviewsReader
//
//  Created by David Halapir on 09/03/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation
import CoreData


extension Comment {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Comment> {
        return NSFetchRequest<Comment>(entityName: "Comment");
    }

    @NSManaged public var text: String?
    @NSManaged public var movie: Movie?

}
