//
//  ButtonProtocol.swift
//  MovieReviewsReader
//
//  Created by David Halapir on 12/03/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation
import UIKit

protocol ButtonDelegate {
    func userDidSaveOrDeleteComment()
}

