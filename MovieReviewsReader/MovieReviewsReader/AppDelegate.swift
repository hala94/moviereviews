//
//  AppDelegate.swift
//  MovieReviewsReader
//
//  Created by David Halapir on 24/02/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
//        // delete this 2 lines after development process
//        CoreDataController.deleteAllData(entity: "Movie")
//        CoreDataController.deleteAllData(entity: "Comment")
        
        window = UIWindow(frame: UIScreen.main.bounds)
        let movievc = MovieViewController()
        let navvc = UINavigationController(rootViewController: movievc)
        navvc.navigationBar.isTranslucent = false
        window?.rootViewController = navvc
        window?.makeKeyAndVisible()
        return true
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        let root = window?.rootViewController
        if let navvc = root as? UINavigationController{
            if let movievc = navvc.topViewController as? MovieViewController {
                movievc.fetchReviews()
            }
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        CoreDataController.saveContext()  // not called for some reason
    }

}

